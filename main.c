#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LENGTH_DESCRIPTION 255
#define MAX_LENGTH_STRUCT 50



struct data {
    int id;
    char description[MAX_LENGTH_DESCRIPTION];
    float value;
}products[MAX_LENGTH_STRUCT];

void createData(int index);
void listing(int index);
void read (int index);
void update (int index);
int deleteData(int index);


int main(void) {
    int option;
    int index; index = 0;


    do {
        
        printf("\n\n*** MENU ***\n\n");
        printf("1. Inclusão\n"); //Create
        printf("2. Listagem\n"); //Listar todos os dados cadastrados nos vetores
        printf("3. Consultar\n"); //Read
        printf("4. Alterar\n"); //Update
        printf("5. Excluir\n");
        printf("0. Sair\n\n");
        printf("Digite sua opcao: ");
        scanf("%d", &option);
        fflush(stdin);

        if(option == 1){
          createData(index);
          index++;
        }else if(option == 2){
          listing(index);
        }else if(option == 3){
          read(index);
        }else if(option ==4){
          update(index);
        }else if(option == 5){
           	deleteData(index);
            index--;
        }
    } while (option != 0);
}



void createData(int index){

  
      printf("produto %d:\n", index+1);
      printf("\n\nInsira o ID do produto \n");
      scanf("%i", &products[index].id);
      if(products[index].id < 0){
        do{
        printf("Nao tem como definir um id negativo");
         scanf("%i", &products[index].id);
        }while(products[index].id < 0);
      }
      fflush(stdin);
      printf("\n");

      printf("Insira a descricao do produto ");
      scanf("%s", products[index].description);
      fflush(stdin);
      printf("\n");

      printf("Insira o valor do produto");
      scanf("%f", &products[index].value);
      if(products[index].value < 0.0){
        do{
        printf("Nao tem como definir um id negativo");
        scanf("%i", &products[index].id);
        }while(products[index].value < 0.0);
      }
      fflush(stdin);
      printf("\n");
    
  
}


void listing(int index){
   int i;
   
   for(i=0; i < index; i++){
       printf("ID %d : %d\n", i+1,products[i].id);
       printf("DESCRCAO %d : %s .\n", i+1, products[i].description);
       printf("VALOR %d : %3.f .\n", i+1, products[i].value);
       printf("\n\n");
      
   }
}


void read (int index){
  int idRead;
  int i;
  int cont;

  cont = 0;

  printf("Digite o ID que deseja buscar: ");
  scanf("%d", &idRead);

  for(i=0 ; i <= index ; i++){
     if(idRead == products[i].id){
       cont = 1;
      printf("PRODUTO ENCONTRADO:\n\n");
      printf("DESCRICAO: %s\n",products[i].description);
      printf("VALOR: %.2f",products[i].value);

     }
  }
  if(cont == 0){
    printf("PRODUTO NAO ENCONTRADO");
  }
 
} 

void update (int index){
   int idUpdate;
  int i;
  int control;

  control = 0;

  printf("Digite o ID que deseja alterar: ");
  scanf("%d", &idUpdate);

  for(i=0 ; i <= index ; i++){
    if(idUpdate == products[i].id){
      control = 1;
      printf("PRODUTO ENCONTRADO!\n");
      printf("DESCRICAO: %s\n",products[i].description);
      printf("VALOR: %.2f\n\n",products[i].value);
      printf("Nova Descricao: ");
      scanf("%s", products[i].description);
      fflush(stdin);
      printf("Novo valor: ");
      scanf("%f", &products[i].value);
    }
  }

  if(control == 0){
    printf("PRODUTO NAO ENCONTRADO");
  }
 
} 


int deleteData(int index){
  int control;
  int product_id, i;
  

  control = 0;
  printf("Insira o ID do produto que deseja remover");
  scanf("%i", &product_id);
  fflush(stdin);
  for(i = 0;i != index; i++){
    if(product_id == products[i].id ){
      control = 1;
      products[i].id = products[i + 1].id; 
      for(int j = 0; j < strlen(products[i].description); j++){
      products[i].description[j] = products[i+1].description[j]; 
      }
      products[i].value = products[i + 1].value; 
      printf("Produto deletado.");
    }
  }
  if(control ==0){
    printf("produto nao encontrado");
    return 1;
  }
  return 0;
}
